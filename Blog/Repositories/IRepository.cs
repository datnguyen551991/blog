﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Blog.Models.DomainModels;

namespace Blog.Repositories
{
    interface IRepository<T>
    {
        T GetById(int Id);
        void Add(T item);
        ICollection<T> GetAll();
        void Delete(int Id);
        void Update(T item);
    }
}