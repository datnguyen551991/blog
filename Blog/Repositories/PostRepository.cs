﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Blog.Models.DomainModels;
using Blog.Mappers;

namespace Blog.Repositories
{
    class PostRepository : IRepository<Post>
    {
        BlogDataContext db;

        public Post GetById(int postId)
        {
            using (db = new BlogDataContext())
            {
                var post = db.Posts.Where(p => p.ID == postId).FirstOrDefault();
                return PostMap.CreateFrom(post);
            }
        }

        public void Add(Post post)
        {
            using (db = new BlogDataContext())
            {
                db.Posts.InsertOnSubmit(new DAM.Post
                {
                    Title = post.Title,
                    Text = post.Text,
                    PostDate = DateTime.Now
                });
                db.SubmitChanges();
            }
        }

        public ICollection<Post> GetAll()
        {
            using (db = new BlogDataContext())
            {
                ICollection<Post> list = new List<Post>();
                foreach (var post in db.Posts.ToList())
                {
                    list.Add(PostMap.CreateFrom(post));
                }
                return list.OrderByDescending(p => p.PostDate).ToList();
            }
        }

        public ICollection<Post> GetByTagId(int tagId)
        {
            using (db = new BlogDataContext())
            {
                ICollection<Post> list = new List<Post>();
                var hasPosts = db.TagPosts.Any(p => p.TagID == tagId);
                if (hasPosts)
                {
                    foreach (var tagPost in db.TagPosts.Where(p => p.TagID == tagId).ToList())
                    {
                        var post = db.Posts.Where(p => p.ID == tagPost.PostID).First();
                        list.Add(PostMap.CreateFrom(post));
                    }
                }
                return list;
            }
        }

        public void Delete(int postId)
        {
        }

        public void Update(Post post)
        {
        }
    }
}