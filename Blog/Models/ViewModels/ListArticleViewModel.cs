﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Blog.Models.DomainModels;

namespace Blog.Models.ViewModels
{
    public class ListArticleViewModel
    {
        public ICollection<ArticleViewModel>listArticles{set;get;}
        public int currentPage { set; get; }
        public int totalPage { set; get; }
    }
}