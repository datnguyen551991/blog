﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Blog.Models.DomainModels;
using Blog.Mappers;

namespace Blog.Repositories
{
    class CommentRepository : IRepository<Comment>
    {
        BlogDataContext db;

        public Comment GetById(int commentId)
        {
            using (db = new BlogDataContext())
            {
                var comment = db.Comments.Where(p => p.ID == commentId).FirstOrDefault();
                return CommentMap.CreateFrom(comment);
            }
        }

        public void Add(Comment comment)
        {
            using (db = new BlogDataContext())
            {
                db.Comments.InsertOnSubmit(new DAM.Comment
                {
                    Text = comment.Text,
                    PostID=comment.PostId,
                    CreatorEmail=comment.CreatorEmail,
                    PostDate = DateTime.Now
                });
                db.SubmitChanges();
            }
        }

        public ICollection<Comment> GetAll()
        {
            return null;
        }

        public ICollection<Comment> GetByPostId(int postId)
        {
            using (db = new BlogDataContext())
            {
                ICollection<Comment> list = new List<Comment>();
                var hasComments = db.Comments.Any(c=>c.PostID==postId);
                if (hasComments)
                {
                    foreach (var comment in db.Comments.Where(c => c.PostID == postId).OrderByDescending(c => c.PostDate).ToList())
                    {
                        list.Add(CommentMap.CreateFrom(comment));
                    }
                }
                return list;
            }
        }

        public void Delete(int commentId)
        {
            using (db = new BlogDataContext())
            {
                var comment=db.Comments.Where(c=>c.ID==commentId).First();
                db.Comments.DeleteOnSubmit(comment);
                db.SubmitChanges();
            }
        }

        public void Update(Comment comment)
        {
        }
    }
}