﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Blog.Models.DomainModels
{
    public class Tag
    {
        public int Id { set; get; }
        public string Name { set; get; }
    }
}