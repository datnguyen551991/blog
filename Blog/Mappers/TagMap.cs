﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Blog.Models.DomainModels;

namespace Blog.Mappers
{
    public class TagMap
    {
        public static Tag CreateFrom(DAM.Tag tag)
        {
            return new Tag
            {
                Id = tag.ID,
                Name = tag.Name
            };
        }
    }
}