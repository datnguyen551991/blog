﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Blog.Models.DomainModels;

namespace Blog.Models.ViewModels
{
    public class ArticleViewModel
    {
        public Post post { set; get; }
        public ICollection<Comment> comments { set; get; }
        public ICollection<Tag> tags { set; get; }
    }
}