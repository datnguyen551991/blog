﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Blog.Models.DomainModels;
using Blog.Mappers;

namespace Blog.Repositories
{
    public class TagRepository : IRepository<Tag>
    {
        BlogDataContext db;

        public Tag GetById(int tagId)
        {
            using (db = new BlogDataContext())
            {
                var tag = db.Tags.Where(t => t.ID == tagId).FirstOrDefault();
                return TagMap.CreateFrom(tag);
            }
        }

        public void Add(Tag tag)
        {
            using (db = new BlogDataContext())
            {
                db.Tags.InsertOnSubmit(new DAM.Tag
                {
                    Name = tag.Name
                });
                db.SubmitChanges();
            }
        }

        public ICollection<Tag> GetByPostId(int postId)
        {
            using (db = new BlogDataContext())
            {
                ICollection<Tag> list = new List<Tag>();
                var hasTags = db.TagPosts.Any(t => t.PostID == postId);
                if (hasTags)
                {
                    foreach (var tagPost in db.TagPosts.Where(t => t.PostID == postId).ToList())
                    {
                        if (db.Tags.Any(t => t.ID == tagPost.TagID))
                        {
                            var tag = db.Tags.Where(t => t.ID == tagPost.TagID).First();
                            list.Add(TagMap.CreateFrom(tag));
                        }
                    }
                }
                return list;
            }
        }

        public ICollection<Tag> GetAll()
        {
            return null;
        }

        public void Delete(int tagId)
        {
        }

        public void Update(Tag tag)
        {
        }
    }
}