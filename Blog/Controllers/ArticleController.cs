﻿using Blog.Models.DomainModels;
using Blog.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Blog.Controllers
{
    public class ArticleController : Controller
    {
        //
        // GET: /Article/
        PostRepository postRepos;
        CommentRepository commentRepos;


        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ViewComments(int postId)
        {
            commentRepos = new CommentRepository();
            return PartialView("_Comments", commentRepos.GetByPostId(postId));
        }

        [HttpPost]
        public ActionResult AddComment(int postId,string userEmail,string commentText)
        {
            commentRepos = new CommentRepository();
            commentRepos.Add(new Comment
            {
                Text = commentText,
                PostDate=DateTime.Now,
                CreatorEmail = userEmail,
                PostId=postId
            });
            return PartialView("_Comments", commentRepos.GetByPostId(postId));
        }

        [HttpPost]
        public ActionResult DeleteComment(int postId,int commentId)
        {
            commentRepos = new CommentRepository();
            commentRepos.Delete(commentId);
            return PartialView("_Comments", commentRepos.GetByPostId(postId));
        }
    }
}
