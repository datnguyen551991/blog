﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Blog.Models.DomainModels
{
    public class Comment
    {

        public int Id { set; get; }

        [Required]
        [AllowHtml]
        public string Text { set; get; }

        [Required]
        public string CreatorEmail { set; get; }

        public DateTime PostDate { set; get; }

        public int PostId { set; get; }
    }
}