﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Blog.Models.DomainModels;
namespace Blog.Mappers
{
    public class CommentMap
    {
        public static Comment CreateFrom(DAM.Comment comment){
            return new Comment
            {
                Id=comment.ID,
                Text=comment.Text,
                PostDate=comment.PostDate,
                CreatorEmail=comment.CreatorEmail,
                PostId=comment.PostID
            };
        }
    }
}