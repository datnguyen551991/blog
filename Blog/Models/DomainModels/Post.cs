﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Blog.Models.DomainModels
{
    public class Post
    {

        public int Id { set; get; }

        [Required(ErrorMessage="Missing Title!!")]
        public string Title { set; get; }

        [Required(ErrorMessage = "Missing Article Content!!")]
        [AllowHtml]
        public string Text { set; get; }

        public DateTime PostDate { set; get; }
    }
}