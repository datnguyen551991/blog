﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Blog.Models.DomainModels;
using Blog.Repositories;

namespace Blog.Controllers
{
    [Authorize]
    public class AdminController : Controller
    {
        //
        // GET: /Admin/

        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult AddPost()
        {
           
            return View();
        }

        [HttpPost]
        public ActionResult AddPost(Post post)
        {
            PostRepository postRepos=new PostRepository();
            postRepos.Add(post); 
            return RedirectToAction("Index","Home");
        }
        public ActionResult ListPost()
        {
            PostRepository postRepos = new PostRepository();
            ICollection<Post> model = postRepos.GetAll();
            return View();
        }


    }
}
