﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Blog.Repositories;
using Blog.Models.ViewModels;
using Blog.Models.DomainModels;

namespace Blog.Controllers
{
    public class HomeController : Controller
    {
        PostRepository postRepos;
        CommentRepository commentRepos;
        TagRepository tagRepos;

        public ActionResult Index(int? page,int?tagId)
        {
            postRepos = new PostRepository();
            commentRepos = new CommentRepository();
            tagRepos = new TagRepository();

            int pageSize = 6;
            int pageNumber = (page == null) ? 1 : (int)page;
            var postList = (tagId == null) ? postRepos.GetAll().ToList() : postRepos.GetByTagId((int)tagId).ToList();

            ICollection<ArticleViewModel> listArticles = new List<ArticleViewModel>();
            foreach (var post in postList.Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList())
            {
                listArticles.Add(new ArticleViewModel
                {
                    post = post,
                    comments = commentRepos.GetByPostId(post.Id),
                    tags=tagRepos.GetByPostId(post.Id)
                });
            }

            ListArticleViewModel model = new ListArticleViewModel
            {
                listArticles = listArticles,
                totalPage = Convert.ToInt32(Math.Ceiling((decimal)postList.Count / pageSize)),
                currentPage = pageNumber
            };
            return View(model);
        }

        public ActionResult About()
        {
            return View();
        }

        public ActionResult LastPost()
        {
            postRepos = new PostRepository();
            int numOfPost = 4;
            ICollection<Post> lastPosts = new List<Post>();
            foreach (var post in postRepos.GetAll().Take(numOfPost))
            {
                lastPosts.Add(post);
            }
            return PartialView("_LastPosts", lastPosts);
        }

        public ActionResult Article(int id)
        {
            postRepos = new PostRepository();
            commentRepos = new CommentRepository();
            tagRepos = new TagRepository();

            ArticleViewModel model = new ArticleViewModel
            {
                post = postRepos.GetById(id),
                comments = commentRepos.GetByPostId(id),
                tags=tagRepos.GetByPostId(id)
            };
            return View(model);
        }
    }
}
