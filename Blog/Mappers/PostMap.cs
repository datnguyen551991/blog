﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Blog.Models.DomainModels;

namespace Blog.Mappers
{
    public class PostMap
    {
        public static Post CreateFrom(DAM.Post post)
        {
            return new Post
            {
                Id = post.ID,
                Title = post.Title,
                Text = post.Text,
                PostDate = post.PostDate,
            };
        }
    }
}